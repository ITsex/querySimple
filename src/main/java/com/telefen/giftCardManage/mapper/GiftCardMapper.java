package com.telefen.giftCardManage.mapper;

import com.telefen.giftCardManage.entity.CardInfo;
import org.springframework.stereotype.Repository;

import java.util.List;
public interface GiftCardMapper {

    List<CardInfo> queryCard();
}
