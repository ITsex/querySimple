package com.telefen.giftCardManage.service;

import com.telefen.giftCardManage.entity.CardInfo;
import com.telefen.giftCardManage.mapper.GiftCardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GiftCardService {
    @Autowired
    private GiftCardMapper giftCardMapper;

    public List<CardInfo> queryCard() {
        List<CardInfo> cardInfos = giftCardMapper.queryCard();
        return cardInfos;
    }
}
