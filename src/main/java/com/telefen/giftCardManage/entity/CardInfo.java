package com.telefen.giftCardManage.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class CardInfo implements Serializable {



    /**
     *   商户ID，发行该券规则的商户
     */
    private String merchantid;

    /**
     *   礼品卡id
     */
    private String codeid;

    /**
     *   券名字
     */
    private String name;

    /**
     *   券副标题
     */
    private String subtitle;

    /**
     *   券类型1：礼品卡2:权益卡3：商品券4：满减券5：现金优惠券6：H5券
     */
    private Integer type;

    /**
     *   是否可以流转 1: 可以流转0：不可以分享
     */
    private Integer cantransfer;

    /**
     *   卡券Logo地址
     */
    private String logourl;

    /**
     *   生效模式：1： 永久有效 2： 指定生效日期 3： 自领取日期起指定天数生效 4： 自领取日期、指定月数生效 5： 自领取日期下个月具体几号生效
     */
    private Integer validmode;

    /**
     *   有效参数，如果是日期类型的，为时间的毫秒时间
     */
    private Long validmodeparam;

    /**
     *   失效模式:1: 永久不失效 2: 指定失效日期3: 自生效日期起指定天数失效 4: 自生效日期起指定月数失效 5: 自生效日期下个月具体几号生效
     */
    private Integer invalidmode;

    /**
     *   失效参数, 如果是日期类型的，为时间的毫秒时间
     */
    private Long invalidmodeparam;

    /**
     *   商品返销的时候是否退回该卡券0：不支持返销1：支持返销
     */
    private Integer refundtype;

    /**
     *   使用是否需要短信验证
     */
    private Integer needsmscheck;

    /**
     *   其它每种类型不一样的使用规则
     */
    private String uselimits;

    /**
     *   券的具体内容，每种券的内容由具体的券内容对象决定
     */
    private String contents;

    /**
     *   如果一个券规则被锁定后是不能修改券的0： 没有锁定 1： 已经锁定
     */
    private Integer islocked;

    /**
     *   创建者
     */
    private String createUserid;

    /**
     *   创建时间
     */
    private Date createTime;

    /**
     *   更新时间
     */
    private Date upateTime;

    /**
     *   是否删除
     */
    private Long isDeleted;

    /**
     *   最后修改人的用户ID
     */
    private String lastUserid;

    /**
     *   业务类型
     */
    private Integer busitype;

    /**
     *   平台
     */
    private String platform;

    /**
     *   税率
     */
    private String invoicerates;

    /**
     *   税收分类编码
     */
    private String taxno;

    /**
     *   券介绍
     */
    private String description;

//    private static final long serialVersionUID = 1L;

}