package com.telefen.giftCardManage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.telefen.giftCardManage.mapper")
public class GiftCardManageApplication {

	public static void main(String[] args) {
		SpringApplication.run(GiftCardManageApplication.class, args);
	}

}
